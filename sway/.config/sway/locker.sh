#!/bin/bash
# Times the screen off and moves it to the background
swayidle \
  timeout 10 'swaymsg "output * dpms off"' \
  resume 'swaymsg "output * dpms on"' &

# Lock the screen immediately
swaylock

# Kill the last background task so the idle timer doesn't keep running
kill %%
