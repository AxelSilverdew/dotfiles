# Set the shell to zsh
export SHELL=/bin/zsh

# Source Antigen
source $HOME/.dotfiles/antigen.zsh

# Lines configured by zsh-newuser-install
HISTFILE=$HOME/.local/zsh_history
HISTSIZE=10000
SAVEHIST=$HISTSIZE
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename '/home/abhiram/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

#Antigen Stuff

# test load the oh-my-zsh library
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle pip
antigen bundle dotenv
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
antigen theme minimal

# Tell antigen that you're done.
antigen apply

#Exports
export TERM="xterm-256color"
export EDITOR="nvim"
export VAGRANT_DEFAULT_PROVIDER=virtualbox
unsetopt nomatch

#Aliases
alias urlshort='curl -F c=@- https://ptpb.pw/u <<<'
alias mp3down='youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail'
alias eclipse="GTK_THEME=Adwaita eclipse"

# Android Studio Exports
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH=$PATH:/opt/gradle/gradle-7.0/bin
export SPICETIFY_INSTALL="/home/abhiram/spicetify-cli"
export PATH="$SPICETIFY_INSTALL:$PATH"
export PATH="/opt/flutter/bin:$PATH"
export PATH="$PATH":"/opt/flutter/.pub-cache/bin"

# Rust Exports
export PATH="$HOME/.cargo/bin:$PATH"

# Makes docker-compose work without root
export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/podman/podman.sock"

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/terraform terraform

export PATH=$PATH:$HOME/.local/bin

eval "$(starship init zsh)"
