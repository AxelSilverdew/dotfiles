#!/usr/bin/env bash
PATH="/opt/flutter/bin:$PATH"
PATH="$PATH":"/opt/flutter/.pub-cache/bin"

# Rust Exports
PATH="$HOME/.cargo/bin:$PATH"
PATH=$PATH:$HOME/.local/bin

neovide
