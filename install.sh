#!/bin/sh
RPMPKGS="git tmux zsh mc stow python-psutil libstdc++-devel libstdc++-static g++ nodejs"

# Crude multi-os installation option
if [ -x "/usr/bin/dnf" ]
then
   sudo dnf install -y $RPMPKGS
elif [ -x "/usr/bin/yum" ]
then
   sudo yum install -y $RPMPKGS
fi

# Add missing directory layout
if [ ! -d "~/.config" ]
then
  mkdir -p ~/.config
fi

mkdir -p ~/.local/bin

# Personal dotfiles
git clone https://github.com/AxelSilverdew/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
git submodule init
git submodule update

# Install Language Servers for Neovim
sudo npm install -g pyright
sudo npm install -g bash-language-server
sudo npm install -g vscode-css-languageserver-bin
sudo npm install -g dockerfile-language-server-nodejs
sudo npm install -g graphql-language-service-cli
sudo npm install -g vscode-html-languageserver-bin
sudo npm install -g typescript typescript-language-server
sudo npm install -g vscode-json-languageserver
sudo npm install -g vim-language-server
sudo npm install -g yaml-language-server
sudo npm install -g markdownlint
sudo npm install -g dockerfile-language-server-nodejs

# stow the configurations
stow alacritty
stow home
stow mpd
stow ncmpcpp
stow themes
stow tmux
stow zsh
stow sway
stow waybar
stow wofi
stow nvim
