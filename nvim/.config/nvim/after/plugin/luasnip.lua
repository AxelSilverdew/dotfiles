local ls = require("luasnip")

-- This is a snippet creator
-- s(<trigger>, <nodes>)
local s = ls.s

-- This is a format node
-- It takes a format string and a list of nodes.
-- fmt(<fmt_string>, {...nodes})
local fmt = require("luasnip.extras.fmt").fmt

-- This is an insert node
-- It takes a position (like $1) and optionally some default text
-- i(<position>, [default_text])
local i = ls.insert_node

-- This repeats a node
-- rep(<position>)
local rep = require("luasnip.extras").rep

ls.config.set_config({
  -- This tells LuaSnip to remember to keep around the last snippet.
  -- You can jump back into it even if you move outside the selection.
  history = true,

  -- This makes dynamic snippets update as you type things into them.
  updateevents = "TextChanged,TextChangedI",

  -- Autosnippets
  enable_autosnippets = true,
})

-- This is where I define my snippets
ls.snippets = {
  lua = {
    s("req", fmt("local {} = require('{}')", { i(1, "default"), rep(1) })),
  },
}

require("luasnip/loaders/from_vscode").lazy_load()

-- <C-l> is selecting within a list of options.
vim.keymap.set("i", "<c-l>", function()
  if ls.choice_active() then
    ls.change_choice(1)
  end
end)

-- shortcut to source my luasnips file again, which will reload my snippets
vim.keymap.set("n", "<leader><leader>s", "<cmd>source ~/.config/nvim/after/plugin/luasnip.lua<CR>")
