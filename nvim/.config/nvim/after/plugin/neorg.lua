require("neorg").setup({
  load = {
    ["core.defaults"] = {},
    ["core.integrations.nvim-cmp"] = {},
    ["core.norg.concealer"] = {
      config = {
        folds = false,
      },
    },
  },
})
