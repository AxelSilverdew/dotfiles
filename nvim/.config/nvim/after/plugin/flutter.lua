require("flutter-tools").setup({
  experimental = {
    lsp_derive_paths = true,
  },
  widget_guides = {
    enabled = true,
  },
})
