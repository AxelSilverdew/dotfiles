require("indent_blankline").setup({
  char = "│",
  filetype_exclude = { "dashboard", "help", "terminal" },
  buftype_exclude = { "terminal" },
  show_current_context = false,
  use_treesitter = "v:true",
})
