local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

require("mason").setup()

-- Automatically install the LSPs
require("mason-lspconfig").setup({
  ensure_installed = {
    "ansiblels",
    "bashls",
    "cssls",
    "dockerls",
    "emmet_ls",
    "eslint",
    "intelephense",
    "jdtls",
    "jsonls",
    "pyright",
    "rust_analyzer",
    "sqlls",
    "lua_ls",
    "svelte",
    "tailwindcss",
    "terraformls",
    "tsserver",
    "yamlls",
  },
})

-- Setup the LSP Servers
require("mason-lspconfig").setup_handlers({
  function(server_name)
    require("lspconfig")[server_name].setup({
      capabilities = capabilities,
    })
  end,
  ["intelephense"] = function()
    require("lspconfig").intelephense.setup({
      init_options = {
        globalStoragePath = "/home/abhiram/.config/intelephense",
        licenceKey = "/home/abhiram/.config/intelephense/license.txt",
        clearCache = true,
      },
    })
  end,
  ["rust_analyzer"] = function()
    require("rust-tools").setup({})
  end,
  ["lua_ls"] = function()
    require("lspconfig").lua_ls.setup({
      settings = {
        Lua = {
          runtime = {
            version = "LuaJIT",
          },
          diagnostics = {
            -- Get rid of the vim warnings
            globals = { "vim" },
          },
          workspace = {
            -- Make the server aware of Neovim runtime files
            library = vim.api.nvim_get_runtime_file("", true),
          },
          telemetry = { enable = false },
        },
      },
    })
  end,
  ["tsserver"] = function()
    require("typescript").setup({
      disable_commands = false, -- prevent the plugin from creating Vim commands
      disable_formatting = true, -- disable tsserver's formatting capabilities
      debug = false, -- enable debug logging for commands
      server = {},
    })
  end,
  ["yamlls"] = function()
    require("lspconfig").yamlls.setup({
      settings = {
        yaml = {
          schemas = {
            ["https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.24.1/all.json"] = "**/*.k8s.yml",
          },
        },
      },
    })
  end,
})

-- Set up DAP
require("mason-nvim-dap").setup({
  ensure_installed = { "python", "codelldb", "firefox", "dart", "chrome", "bash" },
  automatic_setup = true,
})
