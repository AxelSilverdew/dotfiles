-- Clear searches
vim.api.nvim_set_keymap("n", "<Leader>h", ":set hlsearch!<CR>", { noremap = true, silent = true })

-- File Explorer and Navigation
vim.api.nvim_set_keymap("n", "<Leader>e", ":Neotree toggle<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<C-l>", ":bnext<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<C-h>", ":bprevious<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Leader>q", ":Bdelete<CR>", { noremap = true })

-- Copy/Paste
vim.api.nvim_set_keymap("v", "<C-c>", '"+yi', { silent = true })
vim.api.nvim_set_keymap("v", "<C-x>", '"+c', { silent = true })
vim.api.nvim_set_keymap("v", "<C-v>", 'c<ESC>"+p', { silent = true })
vim.api.nvim_set_keymap("i", "<C-v>", '<ESC>"+pa', { silent = true })

-- Telescope
vim.api.nvim_set_keymap("n", "<Leader>ff", ":Telescope find_files<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<Leader>fg", ":Telescope live_grep<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<Leader>fb", ":Telescope buffers<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<Leader>fh", ":Telescope help_tags<CR>", { noremap = true })

-- LSP Keybinds
local default_opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "gT", "<cmd>lua vim.lsp.buf.type_definition()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "<space>wa", "<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "<space>wr", "<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "gI", "<cmd>lua vim.lsp.buf.implementation()<CR>", default_opts)

-- Format Code
vim.api.nvim_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.format { async = true }<CR>", default_opts)

-- Rename
vim.api.nvim_set_keymap("n", "gr", "<cmd>lua vim.lsp.buf.rename()<CR>", default_opts)

-- Show Code Actions
vim.api.nvim_set_keymap("n", "ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", default_opts)
vim.api.nvim_set_keymap("v", "ca", "<cmd>lua vim.lsp.buf.range_code_action()<CR>", default_opts)

-- Jump and Show Diagnostics
vim.api.nvim_set_keymap(
  "n",
  "<C-n>",
  "<cmd>lua vim.diagnostic.goto_next({ float = { border = 'rounded' }})<CR>",
  default_opts
)
vim.api.nvim_set_keymap(
  "n",
  "<C-p>",
  "<cmd>lua vim.diagnostic.goto_prev({ float = { border = 'rounded' }})<CR>",
  default_opts
)
vim.api.nvim_set_keymap(
  "n",
  "<leader>cd",
  "<cmd>lua vim.diagnostic.open_float({ popup_opts = { border = 'single' }})<CR>",
  default_opts
)

-- Show Documentation
vim.api.nvim_set_keymap("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", default_opts)

-- Jump to Definition
vim.api.nvim_set_keymap("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", default_opts)

-- Show references
vim.api.nvim_set_keymap("n", "gR", "<cmd>lua vim.lsp.buf.references()<CR>", default_opts)

-- Floating Terminal
vim.api.nvim_set_keymap("n", "<F1>", "<cmd>FloatermToggle<CR>", default_opts)
vim.api.nvim_set_keymap("t", "<F1>", "<cmd>FloatermToggle<CR>", default_opts)

-- Trouble
vim.api.nvim_set_keymap("n", "<leader>tl", "<cmd>TroubleToggle<CR>", default_opts)

-- DAP Keybinds
vim.api.nvim_set_keymap("n", "<F5>", "<cmd>lua require'dap'.continue()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "<F10>", "<cmd>lua require'dap'.step_over()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "<F11>", "<cmd>lua require'dap'.step_into()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "<F12>", "<cmd>lua require'dap'.step_out()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "<leader>db", "<cmd>lua require'dap'.toggle_breakpoint()<CR>", default_opts)
vim.api.nvim_set_keymap(
  "n",
  "<leader>dB",
  "<cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>",
  default_opts
)
vim.api.nvim_set_keymap(
  "n",
  "<leader>dlp",
  "<cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>",
  default_opts
)
vim.api.nvim_set_keymap("n", "<leader>dr", "<cmd>lua require'dap'.repl_open()<CR>", default_opts)
vim.api.nvim_set_keymap("n", "<leader>dl", "<cmd>lua require'dap'.run_last()<CR>", default_opts)
