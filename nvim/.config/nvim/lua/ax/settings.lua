local cmd = vim.cmd

vim.opt.backup = false -- Recommended by CoC
vim.opt.clipboard = "unnamedplus" -- Copy/Paste between nvim and everything else
vim.opt.cmdheight = 2 -- More space for displaying messages
vim.opt.conceallevel = 2 -- Don't conceal stuff in markdown files
vim.opt.cursorline = true -- Enable highlighting of the current line
vim.opt.dir = "/tmp" -- Directory to store the .swp files
vim.opt.expandtab = true -- Convert tabs to spaces
vim.opt.grepprg = "rg --vimgrep" -- What to use while grepping
vim.opt.guifont = "Iosevka:h14" -- Set the font that vim uses
vim.opt.hidden = true -- Required to keep multiple buffers open
vim.opt.hlsearch = false -- Don't highlight all search results
vim.opt.ignorecase = true -- Ignore search term cases
vim.opt.incsearch = true -- Show incremental search results
vim.opt.laststatus = 3 -- Global statusline
vim.opt.mouse = "a" -- Enable your mouse
vim.opt.number = true -- Show line numbers
vim.opt.relativenumber = true -- Show relative line numbers
vim.opt.shiftwidth = 4 -- Change number of space characters inserted for indentation
vim.opt.showmode = false -- Don't show the editor mode
vim.opt.signcolumn = "yes" -- Always show the signcolumn
vim.opt.smartindent = true
vim.opt.softtabstop = 4
vim.opt.splitbelow = true -- Put new windows below the current one
vim.opt.splitright = true -- Put new windows to the right of the current one
vim.opt.swapfile = true
vim.opt.tabstop = 4 -- Insert 4 spaces for a tab
vim.opt.termguicolors = true
vim.opt.timeoutlen = 500 -- Decrease the timeout time
vim.opt.updatetime = 50 -- Faster completion
vim.opt.wrap = false -- Don't wrap lines be default
vim.opt.writebackup = false -- Recommended by CoC

-- Other Options
cmd("set whichwrap+=<,>,[,],h,l")
cmd("set iskeyword+=-") -- Treat dash separated words as a word text object
cmd("set shortmess+=c") -- Don't pass messages to |ins-completion-menu|.

-- Neovide stuff
cmd("let g:neovide_cursor_vfx_mode='railgun'")
