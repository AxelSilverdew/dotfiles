local M = {}

function M.setup(options)
  local nls = require("null-ls")
  nls.setup({
    debounce = 150,
    save_after_format = false,
    sources = {
      nls.builtins.formatting.alejandra,
      nls.builtins.formatting.trim_whitespace.with({
        filetypes = { "text", "sh", "zsh", "yaml", "toml", "make", "conf" },
      }),
      nls.builtins.formatting.ruff,
      nls.builtins.formatting.dart_format,
      nls.builtins.formatting.eslint_d,
      nls.builtins.formatting.isort,
      nls.builtins.formatting.prettierd.with({
        filetypes = {
          "css",
          "graphql",
          "handlebars",
          "html",
          "javascript",
          "javascriptreact",
          "json",
          "jsonc",
          "less",
          "markdown",
          "scss",
          "svelte",
          "typescript",
          "typescriptreact",
          "vue",
          "yaml",
        },
      }),
      nls.builtins.formatting.rustfmt.with({
        extra_args = { "--edition=2021" },
      }),
      nls.builtins.formatting.stylua,
      nls.builtins.diagnostics.eslint_d,
      nls.builtins.diagnostics.luacheck,
      nls.builtins.diagnostics.ruff,
      nls.builtins.diagnostics.statix,
      nls.builtins.diagnostics.hadolint,
    },
    on_attach = options.on_attach,
    capabilities = options.capabilities,
  })
end

function M.has_formatter(ft)
  local sources = require("null-ls.sources")
  local available = sources.get_available(ft, "NULL_LS_FORMATTING")
  return #available > 0
end

return M
