local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({

  -- General Plugins
  "nvim-lua/plenary.nvim",
  "nvim-lua/popup.nvim",
  "moll/vim-bbye",
  "tpope/vim-sleuth",
  {
    "antoinemadec/FixCursorHold.nvim",
    build = function()
      vim.g.cursorhold_updatetime = 100
    end,
  },
  "b3nj5m1n/kommentary",
  "RRethy/vim-illuminate",
  "godlygeek/tabular",
  "lukas-reineke/indent-blankline.nvim",
  {
    "nvim-neorg/neorg",
    build = ":Neorg sync-parsers",
  },
  "kylechui/nvim-surround",

  -- Telescope
  "nvim-telescope/telescope.nvim",

  -- Treesitter
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
  },
  "nvim-treesitter/nvim-treesitter-refactor",
  "nvim-treesitter/nvim-treesitter-textobjects",
  "RRethy/nvim-treesitter-textsubjects",
  "nvim-treesitter/nvim-treesitter-context",
  "nvim-treesitter/playground",

  -- LSP
  "neovim/nvim-lspconfig",
  "williamboman/mason.nvim",
  "williamboman/mason-lspconfig.nvim",
  "jose-elias-alvarez/null-ls.nvim",
  "akinsho/flutter-tools.nvim",
  "simrat39/rust-tools.nvim",
  "jose-elias-alvarez/typescript.nvim",
  "ray-x/lsp_signature.nvim",

  -- DAP
  "mfussenegger/nvim-dap",
  "jayp0521/mason-nvim-dap.nvim",
  "rcarriga/nvim-dap-ui",

  -- Completion
  "windwp/nvim-autopairs",
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      "lspkind-nvim",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-nvim-lua",
      "saadparwaiz1/cmp_luasnip",
      "onsails/lspkind-nvim",
    },
    config = function()
      require("ax.lsp.completion")
    end,
  },
  "mattn/emmet-vim",

  -- Diagnostics
  "folke/trouble.nvim",

  -- Snippets
  "L3MON4D3/LuaSnip",
  "rafamadriz/friendly-snippets",
  "dsznajder/vscode-es7-javascript-react-snippets",

  -- Navigation
  {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v2.x",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "kyazdani42/nvim-web-devicons",
      "MunifTanjim/nui.nvim",
    },
  },

  -- Statusline and Bufferline
  "freddiehaddad/feline.nvim",
  "romgrk/barbar.nvim",

  -- UI and Icons
  -- "glepnir/lspsaga.nvim",
  "stevearc/dressing.nvim",
  "kyazdani42/nvim-web-devicons",
  "ryanoasis/vim-devicons",

  -- Smooth Scrolling
  "karb94/neoscroll.nvim",

  -- Motions
  {
    "ggandor/leap.nvim",
    dependencies = {
      "tpope/vim-repeat",
    },
    config = function()
      require("leap").set_default_keymaps()
    end,
  },

  -- Colors
  {
    "catppuccin/nvim",
    name = "catppuccin",
  },
  "NvChad/nvim-colorizer.lua",

  -- Git
  {
    "TimUntersberger/neogit",
    -- WARN: see https://github.com/TimUntersberger/neogit/issues/349
    commit = "8adf22f103250864171f7eb087046db8ad296f78",
  },
  "lewis6991/gitsigns.nvim",

  -- Terminal
  "voldikss/vim-floaterm",

  -- Hashicorp
  "hashivim/vim-vagrant",
  "hashivim/vim-terraform",
})
