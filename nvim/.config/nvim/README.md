# My Neovim Config

## Notes to people reading my configuration!

Much of the configuration of individual plugins you can find in either:

```
./after/plugin/\*.lua
This is where configuration for plugins live.

They get auto sourced on startup. In general, the name of the file configures
the plugin with the corresponding name.

./lua/ax/\*.lua
This is where configuration/extensions for new style plugins live.

They don't get sourced automatically, but do get sourced by doing something like:

    require("ax.lsp")

or similar. It is generally recommended that people do this so that you don't accidentally
override any of the plugin requires with namespace clashes. So don't just put your configuration
in "./lua/lsp.lua" because then it will override the plugin version of "lsp.lua"
```
